<?php

use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert([
            'title'=>'artical1',
            'body'=>'first',
            'author'=>'Anil',
            'status'=>'active',
            'category_id'=>1
        ]);
        DB::table('articles')->insert([
            'title'=>'artical2',
            'body'=>'second',
            'author'=>'Bob',
            'status'=>'active',
            'category_id'=>1
        ]);
        DB::table('articles')->insert([
            'title'=>'artical3',
            'body'=>'third',
            'author'=>'Ashely',
            'status'=>'active',
            'category_id'=>1
        ]);
    }
}
