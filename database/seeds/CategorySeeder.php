<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name'=>'Current affair',
            'description'=>'abc'
        ]);
        DB::table('categories')->insert([
            'name'=>'Finance',
            'description'=>'jkl'
        ]);DB::table('categories')->insert([
            'name'=>'Education',
            'description'=>'xyz'
        ]);DB::table('categories')->insert([
            'name'=>'Sports',
            'description'=>'mno'
        ]);
    }
}
