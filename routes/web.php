<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
route::prefix('admin')->middleware('auth')->group(function(){
    Route::get('/', 'DashboardController@index');
    route::get('/articles/tooglestatus','ArticleController@tooglestatus');
    route::get('/articles/search/','ArticleController@search')->name('searchArticle');    
    // route::get('/categories/show/{id}','CategoryController@show');
    route::resource('/categories','CategoryController');
    route::resource('/articles','ArticleController');
    route::get('/categories/{id}/showarticles','CategoryController@showarticles');
    route::get('/categories/{id}/addarticles','CategoryController@addarticles');
    route::get('/categories/{id}/viewarticles','CategoryController@viewarticles');
    route::post('/categories/articlesadded','CategoryController@articlesadded');
    route::resource('/reviews','ReviewController');
    // route::post('/admin/logout','LoginController@logout');
});
route::prefix('admin')->group(function(){
    Auth::routes();

});

Route::get('/home', 'HomeController@index')->name('home');

route::get('/','frontend\FrontendController@index');
route::get('/news/{slug}','frontend\FrontendController@getNews');
route::get('/front/search','frontend\FrontendController@search');
route::post('/comment/store','frontend\CommentController@store');
route::get('/ajaxatest', 'frontend\FrontendController@test');

Route::post('/feedback/store','frontend\FeedbackController@store');
route::post('/article/like','frontend\FrontendController@like');
route::get('/article/{slug}','frontend\FrontendController@articleShow');

Route::get('/othernews','ApiController@api');
Route::get('/contactus','frontend\FeedbackController@index');