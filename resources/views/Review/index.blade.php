
@extends('Layout.admin')

@section('body')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
          <h2 class="content-header-title">Basic Tables</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
          <div class="breadcrumb-wrapper col-xs-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.html">Home</a>
              </li>
              <li class="breadcrumb-item"><a href="#">Tables</a>
              </li>
              <li class="breadcrumb-item active">Basic Tables
              </li>
            </ol>
          </div>
        </div>
      </div>
      <div class="content-body"><!-- Basic Tables start -->
<div class="row">
  <div class="col-xs-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Reviews</h4>
              <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
              <div class="heading-elements">
                  <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                      <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                      <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                      <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                  </ul>
              </div>
          </div>
          <div class="card-body collapse in">
              <div class="card-block card-dashboard">
                  <div class="table-responsive">
                    <a href="/admin/reviews/create" class="btn btn-success btn-sm">Create</a><br><br>                      
                      <table class="table">
                          <thead>
                              <tr>
                                  <th>#</th>
                                  <th>Image</th>
                                  <th>Name</th>
                                  <th>Designation</th>
                                  <th>Comment</th>
                              </tr>
                          </thead>
                          <tbody>
                                @foreach($reviews as $review) 
                              <tr>
                                  <th scope="row">{{$loop->iteration}}</th>
                                  <td><img src="/reviews_images/{{$review->image}}" style="height:20px;" alt="{{$review->image}}"/></td>
                                  <td>{{$review->name}}</td>
                                  <td>{{$review->designation}}</td>
                                  <td>{{substr($review->comment,0,20).'..'}}</td>
                                  <td>
                                    <div class="row">
                                        <div class="col-md-3">  
                                    <a href="/admin/reviews/{{$review->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                        </div>
                                        <div class="col-md-3">
                                        <form method="POST" action="/admin/reviews/{{$review->id}}">
                                            {{csrf_field()}}
                                            <input type="text" hidden name="_method" value="DELETE"/>
                                            <input type="submit" class="btn btn-danger btn-sm" value="Delete"/>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                    </td>
                              </tr>
                              @endforeach
                          </tbody>
                      </table>
                      {{$reviews->links()}}
                  </div>
              </div>
             
          </div>
      </div>
  </div>
</div>



      </div>
    </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection