@extends('Layout.admin')

@section('body')
<div class="app-content content container-fluid">
        <div class="content-wrapper">
                <div class="app-content content container-fluid">
                        <div class="content-wrapper">
                          
                          <div class="content-body"><!-- Basic form layout section start -->
                  <section id="basic-form-layouts">
                      <div class="row match-height">
                          <div class="col-md-6">
                              <div class="card">                               
                                  <div class="card-body collapse in">
                                      <div class="card-block">
                                            <form method="POST" action='/admin/reviews/{{$reviews->id}}' class="form">
                                                {{csrf_field()}}
                                                <input type="text" hidden name="_method" value="PUT"/>
                                                <div class="form-body">
                                                  <h4 class="form-section"><i class="icon-head"></i> Editing Reviews</h4>
                                                <input type="text" hidden name="id" value="{{$reviews->id}}"/>
                                                          <div class="form-group">
                                                              <label for="projectinput1"><b>Name</b></label>
                                                          <input type="text" id="projectinput1" class="form-control" value="{{$reviews->name}}" name="name"/>
                                                          </div>

                                                          <div class="form-group">
                                                              <label for="projectinput2"><b>Designation</b></label>
                                                          <input type="text" id="projectinput2" class="form-control" value="{{$reviews->designation}}" name="designation"/>
                                                          </div>
                                                      
                                                          <div class="form-group">
                                                              <label for="projectinput3"><b>Comment</b></label>
                                                          <textarea id="projectinput3" class="form-control" name="comment">{{$reviews->comment}}</textarea>
                                                          </div>

                                                          <div class="form-group">
                                                                <label for="projectinput6"><b>Image</b></label>
                                                            <input type="file" id="projectinput6" class="form-control" value="{{$reviews->image}}" name="image"/>
                                                          </div>
                                                        <input type="submit" class="btn btn-success" value="Edit"/>
                                                </div>
                                            </form>
                                                                                                 
                                        </div>
                                    </div>
                              </div>
                          </div>
                        </div>
                  </section>
                          </div>
                        </div>
                </div>
        </div>
</div>

@endsection