@extends('Layout.admin')
@section('body')

<div class="app-content content container-fluid">
        <div class="content-wrapper">
                <div class="row">
                        <div class="col-xl-3 col-lg-6 col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-block">
                                        <div class="media">
                                            <div class="media-body text-xs-left">            
                                                <h3 class="pink">{{$categories->count()}}</h3>
                                                <span>Category Count</span>
                                            </div>
                                            <div class="media-right media-middle">
                                                <i class="icon-bag2 pink font-large-2 float-xs-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-block">
                                        <div class="media">
                                            <div class="media-body text-xs-left">
                                                <h3 class="teal">{{$articles->count()}}</h3>
                                                <span>Article Count</span>
                                            </div>
                                            <div class="media-right media-middle">
                                                <i class="icon-user1 teal font-large-2 float-xs-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-block">
                                        <div class="media">
                                            <div class="media-body text-xs-left">
                                                <h3 class="deep-orange">64.89 %</h3>
                                                <span>Conversion Rate</span>
                                            </div>
                                            <div class="media-right media-middle">
                                                <i class="icon-diagram deep-orange font-large-2 float-xs-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-xs-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-block">
                                        <div class="media">
                                            <div class="media-body text-xs-left">
                                                <h3 class="cyan">423</h3>
                                                <span>Support Tickets</span>
                                            </div>
                                            <div class="media-right media-middle">
                                                <i class="icon-ios-help-outline cyan font-large-2 float-xs-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            <div class="card" style="height: 440px;">
                <div class="card-body">
                    <div class="card-block">
                        <h4 class="card-title">List Group</h4>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                    <ul class="list-group list-group-flush">
                        @foreach($categories as $category)
                        <li class="list-group-item">
                        <span class="tag tag-default tag-pill bg-primary float-xs-right">{{$category->articles->count()}}</span>{{$category->name}}
                        </li>
                        @endforeach
                    </ul>
                    <div class="card-block">
                        <a href="#" class="card-link">Card link</a>
                        <a href="#" class="card-link">Another link</a>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection