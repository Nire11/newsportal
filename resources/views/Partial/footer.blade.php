
@if(Session::has('message'))
<div class="alert alert-success mb-2" role="alert" style="    position: fixed;bottom: 5px;right: 5px;">
<p>{{Session::get('message')}}</p>
</div>
@endif
@if(Session::has('msg'))
<div class="alert alert-danger mb-2" role="alert" style="    position: fixed;bottom: 5px;right: 5px;">
<p>{{Session::get('msg')}}</p>
</div>
@endif
@if($errors->any())
<div class="alert alert-danger mb-2" role="alert" style="    position: fixed;bottom: 5px;right: 5px;">
  <ul>
    @foreach($errors->all() as $error)
  <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif


<footer class="footer footer-static footer-light navbar-border">
    <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://pixinvent.com" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with <i class="icon-heart5 pink"></i></span></p>
  </footer>

  <!-- BEGIN VENDOR JS-->
  <script src="/app-assets/js/core/libraries/jquery.min.js" type="text/javascript"></script>
  <script src="/app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
  <script src="/app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
  <script src="/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
  <script src="/app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
  <script src="/app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script>
  <script src="/app-assets/vendors/js/ui/jquery.matchHeight-min.js" type="text/javascript"></script>
  <script src="/app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script>
  <script src="/app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="/app-assets/vendors/js/charts/chart.min.js" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN ROBUST JS-->
  <script src="/app-assets/js/core/app-menu.js" type="text/javascript"></script>
  <script src="/app-assets/js/core/app.js" type="text/javascript"></script>
  <!-- END ROBUST JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="/app-assets/js/scripts/pages/dashboard-lite.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->
  @yield('js')
</body>
</html>
