<html>
<h1><b>Articles list</b></h1>
<a href="/categories">Back</a>
<table style="width:50%">
    <thead>
        <tr>
            <td>Title</td>
            <td>Body</td>
            <td>Status</td>
            <td>Category id</td>
            <td>Action</td>
        </tr>
    </thead>
    <tbody>
    @foreach($articles as $article)
        <tr>
            <td>{{$article->title}}</td>
            <td>{{$article->body}}</td>
        <td><a href="/categories/tooglestatus?id={{$article->id}}&category_id={{$article->category_id}}">{{$article->status}}</a></td>
            <td>{{$article->category_id}}</td>
            <td><a href="/categories/{{$article->id}}/edit">Edit</a>
        <form action="/categories/{{$article->id}}/delete">
        {{csrf_field()}}
        <input type="text" hidden name="_method" value="DELETE"/>
        <input type="submit" value="delete"/>
        </form><td>
        </tr>
    @endforeach
    </tbody>
    </table>
</html>