<html>
<a href="/categories">Back</a>
<table style="width:50%">
    <thead>
        <tr>
            <td>Title</td>
            <td>Body</td>
            <td>Status</td>
            <td>Category id</td>
        </tr>
    </thead>
    <tbody>
    @foreach($articles as $article)
        <tr>
            <td>{{$article->title}}</td>
            <td>{{$article->body}}</td>
            <td>{{$article->status}}</td>
            <td>{{$article->category->name}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</html>