@extends('Layout.admin')

@section('body')
<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="app-content content container-fluid">
            <div class="content-wrapper">                    
            <div class="content-body"><!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">
                    <div class="col-md-10">
                        <div class="card">                               
                            <div class="card-body collapse in">
                                <div class="card-block">
                                    <form method="POST" action='/admin/articles/{{$articles->id}}' class="form">
                                        {{csrf_field()}}
                                        <input type="text" hidden name="_method" value="PUT"/>
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="icon-head"></i> Editing Article</h4>
                                                <input type="text" hidden name="id" value="{{$articles->id}}"/>
                                                    <div class="form-group">
                                                        <label for="projectinput1"><b>Title</b></label>
                                                    <input type="text" id="projectinput1" class="form-control" value="{{$articles->title}}" name="title"/>
                                                    </div>
                                                
                                                
                                                    <div class="form-group">
                                                        <label for="projectinput2"><b>Body</b></label>
                                                    <textarea id="projectinput2" class="form-control" name="body">{{$articles->body}}</textarea>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="projectinput3"><b>Author</b></label>
                                                    <input type="text" id="projectinput3" class="form-control" value="{{$articles->author}}" name="author"/>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="projectinput4"><b>Status</b></label>
                                                    <select type="text" id="projectinput4" class="form-control" name="status" value="{{$articles->status}}">
                                                    <option value="active" {{$articles->status=="active"?'selected':''}}>Active</option>
                                                        <option value="inactive" {{$articles->status=="inactive"?'selected':''}}>Inactive</option>
                                                    </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="projectinput5"><b>Category id</b></label>
                                                    <select type="text" id="projectinput5" class="form-control" name="category_id">
                                                    @foreach($categories as $category)
                                                    <option value="{{$category->id}}" {{$articles->category_id==$category->id?'selected':''}}>{{$category->name}}</option>
                                                    @endforeach
                                                    </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="projectinput6"><b>Image</b></label>
                                                    <input type="file" id="projectinput6" class="form-control" value="{{$articles->image}}" name="image"/>
                                                    </div>
                                                <input type="submit" class="btn btn-success" value="Edit"/>
                                        </div>
                                    </form>
                                                                                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
   <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
   <script>
       tinymce.init({
           selector: "textarea",
           
           plugins: [
               "advlist autolink lists link image charmap print preview hr anchor pagebreak",
               "searchreplace wordcount visualblocks visualchars code",
               "insertdatetime media nonbreaking save table contextmenu directionality",
               "emoticons template paste textcolor colorpicker textpattern"
           ],
           toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
           toolbar2: "print preview | forecolor backcolor emoticons | template",
           image_advtab: true,
           templates: [
               {title: 'Newsletter1', description: 'Notice', url: "/templates/newsletter.html"}
             ]
       });
   </script>
  
@endsection











<!-- <html>
    <form method="POST" action='/articles/{{$articles->id}}'>
        {{csrf_field()}}
        <input type="text" hidden name="_method" value="PUT"/>
        <input type="id" hidden value="{{$articles->id}}" name="id"/>
        <input type="text" name="title" value="{{$articles->title}}"/><br><br>
        <textarea name="body">{{$articles->body}}</textarea><br>
        <input type="text" name="author" value="{{$articles->author}}"/><br>
        <select name="status" value="{{$articles->status}}">
                <option value="active">Active</option>
                <option value="inactive">Inactive</option>
        </select><br>
        <select name="category_id">
        @foreach($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
        @endforeach
        </select>
        <input type="submit" value="edit"/>
    </form>
</html> -->