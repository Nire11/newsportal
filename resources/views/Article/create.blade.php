@extends('Layout.admin')

@section('body')
<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="app-content content container-fluid">
            <div class="content-wrapper">        
            <div class="content-body"><!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">
                    <div class="col-md-10">
                        <div class="card">                               
                            <div class="card-body collapse in">
                                <div class="card-block">
                                    <form method="POST" action='/admin/articles' class="form" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="icon-head"></i> Adding Articles</h4>
                                            
                                                
                                                    <div class="form-group">
                                                        <!-- <button id="onClick" type="button">click</button>
                                                        <button id="onClickbye" type="button">bye</button> -->
                                                        <label for="projectinput1" id="titleGet"><b>Title</b></label>
                                                        <!-- <input type="text" id="projectinput1" value="" class="form-control" placeholder="Enter title" name="title"/> -->
                                                        <input type="text" id="project1" value="" class="form-control" placeholder="Enter title" name="title"/>
                                                
                                                    </div>
                                                    <div id="divId">
                                                    </div>
                                                
                                                
                                                    <div class="form-group">
                                                        <label for="projectinput2" id="hideBody"><b>Body</b></label>
                                                        <textarea id="projectinput2" class="form-control" placeholder="Write body of article" name="body"></textarea>
                                                        
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="projectinput3"><b>Author</b></label>
                                                    <input type="text" id="projectinput3" class="form-control" placeholder="Name of author" name="author"/>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="projectinput4"><b>Status</b></label>
                                                    <select id="projectinput4" class="form-control" name="status">
                                                            <option value="active">Active</option>
                                                            <option value="inactive">Inactive</option>
                                                    </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="projectinput5"><b>Category</b></label>
                                                    <select id="projectinput5" class="form-control" name="category_id">
                                                        @foreach($categories as $c)
                                                    <option value="{{$c->id}}" > {{$c->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="projectinput6"><b>Image</b></label>
                                                    <input type="file" id="projectinput6" class="form-control" name="image"/>
                                                    </div>

                                                <input type="submit" class="btn btn-success" value="Create"/>
                                        </div>
                                    </form>
                                                                                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
   <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
   <script>
       tinymce.init({
           selector: "textarea",
           
           plugins: [
               "advlist autolink lists link image charmap print preview hr anchor pagebreak",
               "searchreplace wordcount visualblocks visualchars code",
               "insertdatetime media nonbreaking save table contextmenu directionality",
               "emoticons template paste textcolor colorpicker textpattern"
           ],
           toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
           toolbar2: "print preview | forecolor backcolor emoticons | template",
           image_advtab: true,
           templates: [
               {title: 'Newsletter1', description: 'Notice', url: "/templates/newsletter.html"}
             ]
       });
   </script>
  
@endsection





<!-- @section('js')
    <script>
        $(document).ready(function(){
            console.log('test');
            $("textarea").focus(function(){
                $('#hideBody').hide()
            });
            $('textarea').change(function(){
                $('#hideBody').show()
            });
            var a=$('#projectinput1').val();
            console.log(a);
            $('#projectinput1').val('value is set');
            $("#onClick").click(function(){
                var a=$("#projectinput1").val();
                $('#project1').val(a);
                $("#divId").append('<h1>Hello</h1>');
                $("#divId").before('<i class="fa fa-edit"></i>')
                console.log($(this).attr('id'));
                $('#projectinput1').attr('type','number');
            });
            $('button').click(function(){
                console.log($(this).attr('id'));
            })
            $('#onClickbye').click(function(){
                $('#divId').html('<h1>Bye</h1>');
                console.log('log')
            });
            
            
        });
        </script>
@endsection -->