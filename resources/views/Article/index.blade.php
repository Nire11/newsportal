@extends('Layout.admin')

@section('body')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
          <h2 class="content-header-title" id="basictable">Basic Tables</h2>
          <!-- <button id="btn">click me</button>
          <button id="sow">show</button> -->
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
          <div class="breadcrumb-wrapper col-xs-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.html">Home</a>
              </li>
              <li class="breadcrumb-item" id="tables"><a href="#">Tables</a>
              </li>
              <li class="breadcrumb-item active" >Basic Tables
              </li>
            </ol>
          </div>
        </div>
      </div>
      <div class="content-body"><!-- Basic Tables start -->
<div class="row">
  <div class="col-xs-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Articles</h4>
              <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
              <div class="heading-elements">
                  <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                      <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                      <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                      <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                  </ul>
              </div>
          </div>
          <div class="card-body collapse in float-xs-right">
            <div class="card-block card-dashboard">
                <div class="table-responsive">
                    <form action="{{route('searchArticle')}}" method="get">
                        <input name="key" title="searchfield" id="searchfield" type="text" size="22">
                        <input name="Search" title="Search" id="searchbutton" type="submit" alt="Search" value="Search">
                    </form>
                </div>
            </div>
            </div>
          <div class="card-body collapse in">
              <div class="card-block card-dashboard">
              <p class="card-text">Add edit and remove articles</p>
                  <div class="table-responsive">
                    <a href="/admin/articles/create" class="btn btn-success btn-sm">Create</a><br><br>                      
                      <table class="table">
                          <thead>
                              <tr>
                                  <th>#</th>
                                  <th>Title</th>
                                  <th>Body</th>
                                  <th>Author</th>
                                  <th>Image</th>
                                  <th>Status</th>
                                  <th>Category</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                                @foreach($articles as $article) 
                              <tr>
                                  <th scope="row">{{$loop->iteration}}</th>                                
                                  <td>{{substr($article->title,0,10).'..'}}</td>
                                  <td>{!!substr($article->body,0,20).'..'!!}</td>
                                  <td>{{substr($article->author,0,5).'..'}}</td>
                                  <td><img src="/articles_images/{{$article->image}}" style="height:20px;" alt="{{$article->image}}"/></td>
                              <td><a aid="{{$article->id}}" href="#" id="tgl{{$article->id}}" class="toggleBtn btn {{$article->status=='active'?'btn-success':'btn-danger'}} btn-sm">{{$article->status}}</a></td>
                                  <td>{{$article->category->name}}</td>
                                  <td>
                                    <div class="row">
                                        <div class="col-md-4">  
                                    <a href="/admin/articles/{{$article->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                        </div>
                                        <div class="col-md-4">
                                        <form method="POST" action="/admin/articles/{{$article->id}}">
                                            {{csrf_field()}}
                                            <input type="text" hidden name="_method" value="DELETE"/>
                                            <input type="submit" class="btn btn-danger btn-sm" value="delete"/>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                    </td>
                              </tr>
                              @endforeach
                          </tbody>
                      </table>
                      {{$articles->links()}}
                      <!-- {{Session::get('name')}} -->
                    </div>
              </div>
             
          </div>
      </div>
  </div>
</div>



      </div>
    </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->

 
@endsection


<!-- @section('js')
<script>
        $(document).ready(function(){
           $('#btn').click(function(){
               $('#basictable').fadeOut(1000);
               $.get('/ajaxatest',function(response){
                    console.log(response);
               });
           });
           $('#sow').click(function(){
               $('#basictable').fadeIn(1000);
           });
           $(".toggleBtn").click(function(){
               var id= $(this).attr('aid');
            //    console.log(id);
                $.get('/admin/articles/tooglestatus',{ 'article_id':id},function(response){
                    console.log(response);
                    $("#tgl"+id).text(response);
                    if(response=='active')
                        $("#tgl"+id).attr('class', 'toggleBtn btn btn-success  btn-sm');
                    else
                        $("#tgl"+id).attr('class', 'toggleBtn btn btn-danger btn-sm');
                        
                })
           })
        });
        </script>
@endsection -->