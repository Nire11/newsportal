<html>
<h1>List of Categories</h1>
<table>
    <thead>
        <tr>
            <td>Name</td>
            <td>Description</td>
        </tr>
    </thead>
    <tbody>
        @foreach($categories as $category)
        <tr>
            <td>{{$category->title}}</td>
            <td>{{$category->description}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</html>