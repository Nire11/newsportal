
@extends('Layout.admin')

@section('body')

<div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
          <h2 class="content-header-title">Basic Tables</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
          <div class="breadcrumb-wrapper col-xs-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.html">Home</a>
              </li>
              <li class="breadcrumb-item"><a href="#">Tables</a>
              </li>
              <li class="breadcrumb-item active">Basic Tables
              </li>
            </ol>
          </div>
        </div>
      </div>
      <div class="content-body"><!-- Basic Tables start -->
<div class="row">
  <div class="col-xs-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Categories</h4>
              <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
              <div class="heading-elements">
                  <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                      <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                      <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                      <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                  </ul>
              </div>
          </div>
          <div class="card-body collapse in">
              <div class="card-block card-dashboard">
                  <p class="card-text">Add edit and remove categories </p>
                  <div class="table-responsive">
                    <a href="/admin/categories/create" class="btn btn-success btn-sm">Create</a><br><br>                      
                      <table class="table">
                          <thead>
                              <tr>
                                  <th>#</th>
                                  <th>Name</th>
                                  <th>Description</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                                @foreach($categories as $category) 
                              <tr>
                                  <th scope="row">{{$loop->iteration}}</th>                                
                                  <td>{{$category->name}}</td>
                                  <td>{{substr($category->description,0,20).'..'}}</td>
                                  <td>
                                    <div class="row">
                                        <div class="col-md-2">  
                                    <a href="/admin/categories/{{$category->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                        </div>
                                        <div class="col-md-2">
                                        <form method="POST" action="/admin/categories/{{$category->id}}">
                                            {{csrf_field()}}
                                            <input type="text" hidden name="_method" value="DELETE"/>
                                            <input type="submit" class="btn btn-danger btn-sm" value="delete"/>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                    </td>
                              </tr>
                              @endforeach
                          </tbody>
                      </table>
                      {{$categories->links()}}
                  </div>
              </div>
             
          </div>
      </div>
  </div>
</div>



      </div>
    </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection


<!-- 
{{-- <html>
   <body>
    <h1><b>Categories list</b></h1>
    <a href="/categories/create">Create</a><br><br>
    <table style="width:40%">
    <thead>
        <tr>
            <td><b>Title</b></td>
            <td><b>Description</b></td>
            <td><b>Action</b></td>
        </tr>
    </thead>
    <tbody>
        @foreach($categories as $category)   
        <tr>
            <td><a href="/categories/{{$category->id}}/showarticles">{{$category->name}}</a></td>
            <td>{{$category->description}}</td>
            <td><a href="/categories/{{$category->id}}/edit">Edit</a>
            <a href="/categories/{{$category->id}}/addarticles">Add articles</a>
            <a href="/articles">View articles</a>
            <form method="POST" action="/categories/{{$category->id}}">
                {{csrf_field()}}
                <input type="text" hidden name="_method" value="DELETE"/>
                <input type="submit" value="delete"/>
            </form></td>
        </tr>
        @endforeach
    </tbody>
    </table>
    </body>
</html> --}} -->