@extends('Layout.admin')

@section('body')
<div class="app-content content container-fluid">
        <div class="content-wrapper">
                <div class="app-content content container-fluid">
                        <div class="content-wrapper">
                          
                          <div class="content-body"><!-- Basic form layout section start -->
                  <section id="basic-form-layouts">
                      <div class="row match-height">
                          <div class="col-md-6">
                              <div class="card">                               
                                  <div class="card-body collapse in">
                                      <div class="card-block">
                                            <form method="POST" action='/admin/categories/{{$categories->id}}' class="form">
                                                {{csrf_field()}}
                                                <input type="text" hidden name="_method" value="PUT"/>
                                                <div class="form-body">
                                                  <h4 class="form-section"><i class="icon-head"></i> Editing Category</h4>
                                                <input type="text" hidden name="id" value="{{$categories->id}}"/>
                                                          <div class="form-group">
                                                              <label for="projectinput1"><b>Name</b></label>
                                                          <input type="text" id="projectinput1" class="form-control" value="{{$categories->name}}" name="name"/>
                                                          </div>
                                                      
                                                      
                                                          <div class="form-group">
                                                              <label for="projectinput2"><b>Description</b></label>
                                                          <textarea id="projectinput2" class="form-control" name="description">{{$categories->description}}</textarea>
                                                          </div>
                                                        <input type="submit" class="btn btn-success" value="Edit"/>
                                                </div>
                                            </form>
                                                                                                 
                                        </div>
                                    </div>
                              </div>
                          </div>
                        </div>
                  </section>
                          </div>
                        </div>
                </div>
        </div>
</div>

@endsection
<!-- {{-- <html>
    <form method="POST" action='/categories/{{$categories->id}}'>
        {{csrf_field()}}
        <input type="text" hidden name="_method" value="PUT"/>
        <input type="id" hidden value="{{$categories->id}}" name="id"/>
        <input type="text" name="name" value="{{$categories->name}}"/><br><br>
        <textarea name="description">{{$categories->description}}</textarea><br>
        <input type="submit" value="edit"/>
    </form>
</html> --}} -->