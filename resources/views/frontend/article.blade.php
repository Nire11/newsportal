
@extends('frontend.layout.front')
@section('body')
<style>
    .fa {
    padding: 5px;
    font-size: 15px;
    width: 30px;
    text-align: center;
    text-decoration: none;
    margin: 5px 2px;
    }

    .fa:hover {
        opacity: 0.7;
    }

    .fa-facebook {
    background: #3B5998;
    color: white;
    }

    .fa-twitter {
    background: #55ACEE;
    color: white;
    }

    .fa-google {
    background: #dd4b39;
    color: white;
    }

    .fa-linkedin {
    background: #007bb5;
    color: white;
    }
</style>
<div class="hero-area">
    <div class="container">
        
    </div>
</div>
<!-- ##### Hero Area End ##### -->

<!-- ##### Blog Area Start ##### -->
<div class="blog-area section-padding-0-80">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8">
                <div class="blog-posts-area">
                    <!-- Single Featured Post -->
                    <div class="single-blog-post featured-post mb-30">
                        <div class="post-thumb">
                            <a href="#"><img src="/articles_images/{{$article->image}}" alt=""></a>
                        </div>
                        <input type="text" hidden id="article_id" value="{{$article->id}}"/>
                        <div class="post-data">
                            <h6>{{$article->title}}</h6>
                            <div class="post-meta">
                                <p class="post-author">By <a>{{$article->author}}</a></p>
                                <p class="post-excerp">{!!$article->body!!}</p>
                                <!-- Post Like & Post Comment -->
                                <div class="d-flex align-items-center">
                                    <a href="#" id="like_count" class="post-like"><img src="/frontend-assets/img/core-img/like.png" alt="like">Like <span >{{$article->count}}</span> </a>                                  
                                    <a href="#" id="comment_count" class="post-comment"><img src="/frontend-assets/img/core-img/chat.png" alt="comments">Comments <span>{{$article->comments->count()}} </span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12 col-lg-8">
                        <div class="post-footer-inner">
                            <div class="icon-bar">
                                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a> 
                                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a> 
                                <a href="#" class="google"><i class="fa fa-google"></i></a> 
                                <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                            </div><!-- .share-links /-->
                        </div><!-- .post-footer-inner /-->
                    </div>

                    <!-- Latest Comments Widget -->
                    <div class="latest-comments-widget">
                        <h3>Latest Comments</h3>

                        <!-- Single Comments -->
                        <div class="comments" id="comment_div">
                            @foreach($article->comments as $comment)
                                <div class="single-comments d-flex">
                                    <div class="comments-text">
                                    <a>{{$comment->name}}</a>
                                    <p>{{$comment->comment}}</p>
                                    </div><br>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <h1><u>Comments</u></h1>
                    <form method="GET" action='#' class="form">
                        {{csrf_field()}}
                        <div class="form-body">
                            <div class="form-group">
                                <label for="projectinput1"><b>Name</b></label>
                                <input type="text" id="projectinput1" class="form-control" placeholder="Enter name" name="name"/>
                            </div>
                        
                            <div class="form-group">
                                <label for="projectinput2"><b>Comment</b></label>
                                <textarea id="projectinput2" cols="45" rows="8" class="form-control" placeholder="Comments here......." name="comment"></textarea>
                            </div>
                            <input hidden type="integer" name="article_id"/>
                            <input id="SubBtn" type="button" class="btn btn-success" value="Post Comment"/>
                            <i style="display:none" id="spinner" class="fa fa-spinner fa-spin"></i>
                            <span id="display_error" class="text-danger"></span>
                            <span id="display_success" class="text-success"></span>
                        </div>
                    </form>
                </div>
                <nav aria-label="Page navigation example">
                    <!-- <ul class="pagination mt-50">
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                        <li class="page-item"><a class="page-link" href="#">...</a></li>
                        <li class="page-item"><a class="page-link" href="#">10</a></li>
                    </ul> -->
                </nav>
            </div>
       
            <div class="col-12 col-lg-4">
                <div class="blog-sidebar-area">

                    <!-- Latest Posts Widget -->
                    <div class="latest-posts-widget mb-50">
                        <div class="container">

                        <!-- Single Featured Post -->
                        <div class="single-blog-post small-featured-post d-flex">
                            <div class="post-thumb">
                                <a href="#"><img src="/frontend-assets/img/bg-img/19.jpg" alt=""></a>
                            </div>
                            <div class="post-data">
                            <a href="#" class="post-catagory">Finance</a>
                                <div class="post-meta">
                                <a href="" class="post-title">
                                        <h6>Pellentesque mattis arcu massa, nec fringilla turpis eleifend id.</h6>
                                    </a>
                                    <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                                </div>
                            </div>
                        </div>

                        <!-- Single Featured Post -->
                        <div class="single-blog-post small-featured-post d-flex">
                            <div class="post-thumb">
                                <a href="#"><img src="/frontend-assets/img/bg-img/20.jpg" alt=""></a>
                            </div>
                            <div class="post-data">
                                <a href="#" class="post-catagory">Politics</a>
                                <div class="post-meta">
                                    <a href="#" class="post-title">
                                        <h6>Sed a elit euismod augue semper congue sit amet ac sapien.</h6>
                                    </a>
                                    <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                                </div>
                            </div>
                        </div>

                        <!-- Single Featured Post -->
                        <div class="single-blog-post small-featured-post d-flex">
                            <div class="post-thumb">
                                <a href="#"><img src="/frontend-assets/img/bg-img/21.jpg" alt=""></a>
                            </div>
                            <div class="post-data">
                                <a href="#" class="post-catagory">Health</a>
                                <div class="post-meta">
                                    <a href="#" class="post-title">
                                        <h6>Pellentesque mattis arcu massa, nec fringilla turpis eleifend id.</h6>
                                    </a>
                                    <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                                </div>
                            </div>
                        </div>

                        <!-- Single Featured Post -->
                        <div class="single-blog-post small-featured-post d-flex">
                            <div class="post-thumb">
                                <a href="#"><img src="/frontend-assets/img/bg-img/22.jpg" alt=""></a>
                            </div>
                            <div class="post-data">
                                <a href="#" class="post-catagory">Finance</a>
                                <div class="post-meta">
                                    <a href="#" class="post-title">
                                        <h6>Augue semper congue sit amet ac sapien. Fusce consequat.</h6>
                                    </a>
                                    <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                                </div>
                            </div>
                        </div>

                        <!-- Single Featured Post -->
                        <div class="single-blog-post small-featured-post d-flex">
                            <div class="post-thumb">
                                <a href="#"><img src="/frontend-assets/img/bg-img/23.jpg" alt=""></a>
                            </div>
                            <div class="post-data">
                                <a href="#" class="post-catagory">Travel</a>
                                <div class="post-meta">
                                    <a href="#" class="post-title">
                                        <h6>Pellentesque mattis arcu massa, nec fringilla turpis eleifend id.</h6>
                                    </a>
                                    <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                                </div>
                            </div>
                        </div>

                        <!-- Single Featured Post -->
                        <div class="single-blog-post small-featured-post d-flex">
                            <div class="post-thumb">
                                <a href="#"><img src="/frontend-assets/img/bg-img/24.jpg" alt=""></a>
                            </div>
                            <div class="post-data">
                                <a href="#" class="post-catagory">Politics</a>
                                <div class="post-meta">
                                    <a href="#" class="post-title">
                                        <h6>Augue semper congue sit amet ac sapien. Fusce consequat.</h6>
                                    </a>
                                    <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>
    $(document).ready(function(){
        $("#SubBtn").click(function(){
            $('#spinner').show();
            console.log('test');
            var id=$('#article_id').val();
            var name=$("#projectinput1").val();
            var comment=$("#projectinput2").val();
            console.log(name);
            if(name==''||comment=='')
            {
                $('#display_error').css('display','block');
                $('#display_error').html('All fields are required').fadeOut(7000);
            }
            $.post('/comment/store',{'_token':'{{csrf_token()}}',id:id, 'name':name, 'comment':comment},function(response){
           $('#spinner').hide();
           $('#display_success').css('display','block');
            $('#display_success').html('comment added successfully').fadeOut(7000);
            $("#projectinput1").val('');
            $("#projectinput2").val('');
            $("#comment_div").append(response);
            });
        });
        $("#like_count").click(function(){
            var id=$("#article_id").val();
            console.log(id);
            $.post('/article/like',{'_token':'{{csrf_token()}}', id:id},function(response){
                
                $('#like_count').find('span').text(response);
                $('#like_count').unclick();
            })
        });
        $(".latest-comments-widget").hide();
        $("#comment_count").click(function(){
            // console.log('test');
            $(".latest-comments-widget").toggle();
        })
    });

</script>
@endsection