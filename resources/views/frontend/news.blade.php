@extends('frontend.layout.front')
@section('body')
<div class="hero-area">
    <div class="container">
        
    </div>
</div>
<!-- ##### Hero Area End ##### -->

<!-- ##### Blog Area Start ##### -->
<div class="blog-area section-padding-0-80">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8">
                <div class="blog-posts-area">
                    @foreach($articles as $article)
                    <!-- Single Featured Post -->
                    <div class="single-blog-post featured-post mb-30">
                        <div class="post-thumb">
                            <a href="/article/{{$article->slug}}"><img src="/articles_images/{{$article->image}}" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="#" class="post-catagory">{{$article->category->name}}</a>
                            <a href="/article/{{$article->slug}}" class="post-title">
                            <h6>{{$article->title}}</h6>
                            </a>
                            <div class="post-meta">
                                <p class="post-author">By <a href="#">{{$article->author}}</a></p>
                            <p class="post-excerp">{!!substr($article->body,0,250).'..'!!}</p>
                                <!-- Post Like & Post Comment -->
                                <div class="d-flex align-items-center">
                                    <a href="#" class="post-like"><img src="/frontend-assets/img/core-img/like.png" alt=""> Likes<span>{{$article->count}}</span></a>
                                    <a href="#" class="post-comment"><img src="/frontend-assets/img/core-img/chat.png" alt=""> Comments<span>{{$article->comments->count()}}</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    {{$articles->links()}}
                </div>

            </div>

            <div class="col-12 col-lg-4">
                <div class="blog-sidebar-area">
                    
                    <!-- Latest Posts Widget -->
                    <div class="latest-posts-widget mb-50">
                        @foreach($section_articles as $article)
                        <!-- Single Featured Post -->
                        <div class="single-blog-post small-featured-post d-flex">
                            <div class="post-thumb">
                                <a href="#"><img src="/articles_images/{{$article->image}}" alt=""></a>
                            </div>
                            <div class="post-data">
                            <a href="/article/{{$article->slug}}" class="post-catagory">{{$article->category->name}}</a>
                                <div class="post-meta">
                                <a href="" class="post-title">
                                        <h6>{{$article->title}}</h6>
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>


@endsection

