@extends('frontend.layout.front')

@section('body')
<div class="hero-area">
    <div class="container">
    <!-- <button id="clickButton" class="btn btn-danger btn-md">Click me</button> -->
    <div id="response"></div>
        <div class="row align-items-center">
            <div class="col-12 col-lg-8">
                <!-- Breaking News Widget -->
                <div class="breaking-news-area d-flex align-items-center">
                    <div class="news-title">
                        <p>Breaking News</p>
                    </div>
                    <div id="breakingNewsTicker" class="ticker">
                        <ul>
                            <li><a href="#">Tottenham Hotspur finally opened its shiny new $1.3 billion stadium</a></li>
                            <li><a href="#">Ethiopian Airlines pilots 'followed expected procedures before crash'</a></li>
                            <li><a href="#">World's deepest pool to open in Poland</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Breaking News Widget -->
                <div class="breaking-news-area d-flex align-items-center mt-15">
                    <div class="news-title title2">
                        <p id="hideNews">International</p>
                    </div>
                    <div id="internationalTicker" class="ticker">
                        <ul>
                            <li><a href="#">European Union takes legal action to protect Polish judges</a></li>
                            <li><a href="#">Scientists have found a 'fossil graveyard' linked to the asteroid that killed off the dinosaurs</a></li>
                            <li><a href="#">Enjoy!</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Hero Add -->
            <div class="col-12 col-lg-4">
                <div class="hero-add">
                    <a href="#" id="ad"><img src="/frontend-assets/img/bg-img/hero-add.gif" alt=""></a>
                    <button id="hideAd" class="btn btn-warning btn-sm">remove ad</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### Hero Area End ##### -->

<!-- ##### Featured Post Area Start ##### -->
<div class="featured-post-area">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-8">
                <div class="row">

                    <!-- Single Featured Post -->
                    @foreach($section1_articles as $article)
                    <div class="col-12 col-lg-7">
                        <div class="single-blog-post featured-post">
                            <div class="post-thumb">
                                <a href="/article/{{$article->slug}}"><img src="/articles_images/{{$article->image}}" alt=""></a>
                            </div>
                            <div class="post-data">
                                <a href="#" class="post-catagory">{{$article->category->name}}</a>
                                <a href="/article/{{$article->slug}}" class="post-title">
                                    <h6>{{$article->title}}</h6>
                                </a>
                                <div class="post-meta">
                                    <p class="post-author">By <a href="#">{{$article->author}}</a></p>
                                    <!-- Post Like & Post Comment -->
                                    <div class="d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="/frontend-assets/img/core-img/like.png" alt=""> <span>{{$article->count}}</span></a>
                                        <a href="#" class="post-comment"><img src="/frontend-assets/img/core-img/chat.png" alt=""> <span>{{$article->comments->count()}}</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">

                <!-- Single Featured Post -->
                @foreach($section5_articles as $article)
                <div class="single-blog-post small-featured-post d-flex">
                    <div class="post-thumb">
                        <a href="/article/{{$article->slug}}"><img src="/articles_images/{{$article->image}}" alt=""></a>
                    </div>
                    <div class="post-data">
                        <a href="#" class="post-catagory">{{$article->category->name}}</a>
                        <div class="post-meta">
                            <a href="/article/{{$article->slug}}" class="post-title">
                                <h6>{{$article->title}}</h6>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<!-- ##### Featured Post Area End ##### -->

<!-- ##### Popular News Area Start ##### -->
<div class="popular-news-area section-padding-80-50">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8">
                <div class="section-heading">
                    <h6>Popular News</h6>
                </div>

                <div class="row">
                    @foreach($section2_articles as $article)
                        <!-- Single Post -->
                        <div class="col-12 col-md-6">
                            <div class="single-blog-post style-3">
                                <div class="post-thumb">
                                    <a href="/article/{{$article->slug}}"><img src="/articles_images/{{$article->image}}" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="#" class="post-catagory">{{$article->category->name}}</a>
                                    <a href="/article/{{$article->slug}}" class="post-title">
                                        <h6>{{$article->title}}</h6>
                                    </a>
                                    <div class="post-meta d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="/frontend-assets/img/core-img/like.png" alt=""> <span>{{$article->count}}</span></a>
                                        <a href="#" class="post-comment"><img src="/frontend-assets/img/core-img/chat.png" alt=""> <span>{{$article->comments->count()}}</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-12 col-lg-4">
                <!-- Popular News Widget -->
                <div class="popular-news-widget mb-30">
                    <h3>4 Most Popular News</h3>

                    @foreach($section2_articles as $article)
                    <div class="single-popular-post">
                        <a href="/article/{{$article->slug}}">
                            <h6><span>{{$loop->iteration}}.</span> {{$article->title}} </h6>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### Popular News Area End ##### -->

<!-- ##### Video Post Area Start ##### -->
<div class="video-post-area bg-img bg-overlay" style="background-image: url(img/bg-img/bg1.jpg);">
    <div class="container">
        <div class="section-heading">
            <h6>Popular Videos</h6>
        </div>
        <div class="row justify-content-center">
            <!-- Single Video Post -->
            <div class="col-12 col-sm-6 col-md-4">
                <div class="single-video-post">
                    <img src="/frontend-assets/img/bg-img/video1.jpg" alt="">
                    <!-- Video Button -->
                    <div class="videobtn">
                        <a href="https://www.youtube.com/watch?v=5BQr-j3BBzU" class="videoPlayer"><i class="fa fa-play" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>

            <!-- Single Video Post -->
            <div class="col-12 col-sm-6 col-md-4">
                <div class="single-video-post">
                    <img src="/frontend-assets/img/bg-img/video2.jpg" alt="">
                    <!-- Video Button -->
                    <div class="videobtn">
                        <a href="https://www.youtube.com/watch?v=5BQr-j3BBzU" class="videoPlayer"><i class="fa fa-play" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>

            <!-- Single Video Post -->
            <div class="col-12 col-sm-6 col-md-4">
                <div class="single-video-post">
                    <img src="/frontend-assets/img/bg-img/video3.jpg" alt="">
                    <!-- Video Button -->
                    <div class="videobtn">
                        <a href="https://www.youtube.com/watch?v=5BQr-j3BBzU" class="videoPlayer"><i class="fa fa-play" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### Video Post Area End ##### -->

<!-- ##### Editorial Post Area Start ##### -->
<div class="editors-pick-post-area section-padding-80-50">
    <div class="container">
        <div class="row">
            <!-- Editors Pick -->
            <div class="col-12 col-md-7 col-lg-9">
                <div class="section-heading">
                    <h6>Editor’s Pick</h6>
                </div>

                <div class="row">
                    @foreach($section3_articles as $article)
                        <!-- Single Post -->
                        <div class="col-12 col-lg-4">
                            <div class="single-blog-post">
                                <div class="post-thumb">
                                    <a href="/article/{{$article->slug}}"><img src="/articles_images/{{$article->image}}" alt=""></a>
                                </div>
                                <div class="post-data">
                                    <a href="/article/{{$article->slug}}" class="post-title">
                                        <h6>{{$article->title}}</h6>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <!-- World News -->
            <div class="col-12 col-md-5 col-lg-3">
                <div class="section-heading">
                    <h6>World News</h6>
                </div>
                
                @foreach($section4_articles as $article)
                    <!-- Single Post -->
                    <div class="single-blog-post style-2">
                        <div class="post-thumb">
                            <a href="/article/{{$article->slug}}"><img src="/articles_images/{{$article->image}}" alt=""></a>
                        </div>
                        <div class="post-data">
                            <a href="/article/{{$article->slug}}" class="post-title">
                                <h6>{{$article->title}}</h6>
                            </a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</div>
<!-- ##### Editorial Post Area End ##### -->

@endsection

@section('js')
<script>
    $(document).ready(function(){
        $("#clickButton").click(function(){
        // console.log('hello world');
            $("#hideNews").toggle();
            $("#internationalTicker").toggle();

              $.get('/ajaxatest',{name:'saurav'} ,function(response){
                    console.log(response);                    
                    // $("#response").html(response);
                });
            });
            $("#hideAd").click(function(){
            $("#ad").toggle();
        });
        });
</script>
@endsection