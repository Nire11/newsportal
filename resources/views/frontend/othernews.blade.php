@extends('frontend.layout.front')
@section('body')

<div class="hero-area">
    <div class="container">
        <div class='container'>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="section-heading">
                            <h6>राजनीति</h6>
                        </div>
                        <div class="row">
                            @foreach ($section1 as $news)
                                    <div class="col-12 col-lg-4">
                                        <div class="single-blog-post">
                                            <div class="post-thumb">
                                                <a href="#"><img src="{{$news->news_image}}" alt=""></a>
                                            </div>
                                            <div class="post-data">
                                                <a href="#" class="post-title">
                                                    <h6>{{$news->title}}</h6>
                                                </a>
                                                <div class="post-meta">
                                                    <p class="post-excerp">{!!$news->body1!!}</p>
                                                    <p class="post-date">Date <a>{{$news->date}}</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="row">
                        <div class="section-heading">
                            <h6>खेलकुद</h6>
                        </div>

                        <div class="row">
                            @foreach($section2 as $news)
                                <!-- Single Post -->
                                <div class="col-12 col-lg-4">
                                    <div class="single-blog-post">
                                        <div class="post-thumb">
                                            <a href="#"><img src="{{$news->news_image}}" alt=""></a>
                                        </div>
                                        <div class="post-data">
                                            <a href="#" class="post-title">
                                                <h6>{{$news->title}}</h6>
                                            </a>
                                            <div class="post-meta">
                                                    <p class="post-excerp">{!!$news->body1!!}</p>
                                                    <p class="post-date">Date <a>{{$news->date}}</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>   
                    
                    <div class="row">
                        <div class="section-heading">
                            <h6>विविध</h6>
                        </div>

                        <div class="row">
                            @foreach($section3 as $news)
                                <!-- Single Post -->
                                <div class="col-12 col-lg-4">
                                    <div class="single-blog-post">
                                        <div class="post-thumb">
                                            <a href="#"><img src="{{$news->news_image}}" alt=""></a>
                                        </div>
                                        <div class="post-data">
                                            <a href="#" class="post-title">
                                                <h6>{{$news->title}}</h6>
                                            </a>
                                            <div class="post-meta">
                                                    <p class="post-excerp">{!!$news->body1!!}</p>
                                                    <p class="post-date">Date <a>{{$news->date}}</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="section-heading">
                            <h6>विचार|ब्लग</h6>
                        </div>

                        <div class="row">
                            @foreach($section4 as $news)
                                <!-- Single Post -->
                                <div class="col-12 col-lg-4">
                                    <div class="single-blog-post">
                                        <div class="post-thumb">
                                            <a href="#"><img src="{{$news->news_image}}" alt=""></a>
                                        </div>
                                        <div class="post-data">
                                            <a href="#" class="post-title">
                                                <h6>{{$news->title}}</h6>
                                            </a>
                                            <div class="post-meta">
                                                    <p class="post-excerp">{!!$news->body1!!}</p>
                                                    <p class="post-date">Date <a>{{$news->date}}</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                                        
                    <div class="row">
                        <div class="section-heading">
                            <h6>रोचक</h6>
                        </div>

                        <div class="row">
                            @foreach($section5 as $news)
                                <!-- Single Post -->
                                <div class="col-12 col-lg-4">
                                    <div class="single-blog-post">
                                        <div class="post-thumb">
                                            <a href="#"><img src="{{$news->news_image}}" alt=""></a>
                                        </div>
                                        <div class="post-data">
                                            <a href="#" class="post-title">
                                                <h6>{{$news->title}}</h6>
                                            </a>
                                            <div class="post-meta">
                                                    <p class="post-excerp">{!!$news->body1!!}</p>
                                                    <p class="post-date">Date <a>{{$news->date}}</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                                        
                    <div class="row">
                        <div class="section-heading">
                            <h6>अर्थ|बाणिज्य</h6>
                        </div>

                        <div class="row">
                            @foreach($section6 as $news)
                                <!-- Single Post -->
                                <div class="col-12 col-lg-4">
                                    <div class="single-blog-post">
                                        <div class="post-thumb">
                                            <a href="#"><img src="{{$news->news_image}}" alt=""></a>
                                        </div>
                                        <div class="post-data">
                                            <a href="#" class="post-title">
                                                <h6>{{$news->title}}</h6>
                                            </a>
                                            <div class="post-meta">
                                                    <p class="post-excerp">{!!$news->body1!!}</p>
                                                    <p class="post-date">Date <a>{{$news->date}}</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                                        
                    <div class="row">
                        <div class="section-heading">
                            <h6>कला|साहित्य</h6>
                        </div>

                        <div class="row">
                            @foreach($section7 as $news)
                                <!-- Single Post -->
                                <div class="col-12 col-lg-4">
                                    <div class="single-blog-post">
                                        <div class="post-thumb">
                                            <a href="#"><img src="{{$news->news_image}}" alt=""></a>
                                        </div>
                                        <div class="post-data">
                                            <a href="#" class="post-title">
                                                <h6>{{$news->title}}</h6>
                                            </a>
                                            <div class="post-meta">
                                                    <p class="post-excerp">{!!$news->body1!!}</p>
                                                    <p class="post-date">Date <a>{{$news->date}}</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection