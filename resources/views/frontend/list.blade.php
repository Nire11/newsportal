@extends('frontend.layout.front')

@section('body')

<div class="blog-area section-padding-0-80">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8">
                <div class="blog-posts-area">
                    @foreach($categories as $category)
                    <!-- Single Featured Post -->
                    <div class="single-blog-post featured-post mb-30">
                        <div class="post-data">
                            <h1><a href="/news/{{$category->slug}}">{{$category->name}}</a></h1>
                        </div>
                    </div>
                    @endforeach
                    {{$categories->links()}}
                </div>
            </div>
        </div>

            </div>
        </div>
    </div>
</div>
@endsection

