    <!-- ##### Header Area Start ##### -->
    <header class="header-area">

        <!-- Top Header Area -->
        <div class="top-header-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="top-header-content d-flex align-items-center justify-content-between">
                            <!-- Logo -->
                            <div class="logo">
                                <a href="index.html"><img src="/frontend-assets/img/core-img/logo.png" alt=""></a>
                            </div>

                            <!-- Login Search Area -->
                            <div class="login-search-area d-flex align-items-center">                          
                                <!-- Search Form -->
                                <div class="search-form">
                                    <form action="/front/search/" method="get">
                                        <input type="search" name="search" class="form-control" placeholder="Search">
                                        <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Navbar Area -->
        <div class="newspaper-main-menu" id="stickyMenu">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="newspaperNav">

                        <!-- Logo -->
                        <div class="logo">
                            <a href="index.html"><img src="/frontend-assets/img/core-img/logo.png" alt=""></a>
                        </div>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- close btn -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav" id="asd">
                                <ul>
                                    <li class="active"><a href="/">Home</a></li>
                                    <li><a href="/news/politics">Politics</a></li>                   
                                    <li><a href="/news/lifestyle">Lifestyle</a></li>                   
                                    <li><a href="/news/travel">Travel</a></li>                   
                                    <li><a href="/news/sports">Sports</a></li>                   
                                    <li><a href="/othernews">Other Links</a></li>                                       
                                    <li><a href="/contactus">Contact Us</a></li>
                                </ul>
                            </div>
                            <!-- Nav End -->
                        </div>
                        <div class="social">
                            <a href="https://www.facebook.com/" target="_blank"><span class="fa fa-facebook btn-md"></span></a>
                            <a href="https://twitter.com/" target="_blank"><span class="fa fa-twitter btn-lg"></span></a>
                            <a href="#" target="_blank"><span class="fa fa-google btm-lg"></span></a>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
