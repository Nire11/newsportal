@extends('frontend.layout.front')

@section('body')
<div class="hero-area">
    <div class="container">

        <div class="footer-widget-area mt-80">
            <!-- Title -->
            <h1 class="widget-title">Contact Detail</h1>
            <!-- List -->
            <ul class="list">
                <li><b>Mail us:</b><a href="mailto:contact@youremail.com">contact@enewspaper.com</a></li>
                <li><b>Location:</b><a href="">Kathmandu, Nepal</a></li>
                <li><b>Contact No:</b><a href="+43 5278 2883 884">+43 5278 2883 884</a></li>
            </ul>
        </div>
    </div>
</div>

    <div class="container">
        <!-- Feedback  -->
        <div class="section-heading">
            <h1>Feedbacks</h1>
            <p>If any feedback please send us<br>Note: Your email and phone number will not be published</p>
        </div>

        <form method="POST" action='#' class="formSbmt">
            {{csrf_field()}}
            <div class="form-body">
                        <div class="form-group">
                            <label for="projectinput1"><b>First Name</b></label>
                            <input type="text" id="projectinput1" class="form-control" placeholder="Enter first name" name="fname"/>
                        </div>
                        <div class="form-group">
                            <label for="projectinput2"><b>Last Name</b></label>
                            <input type="text" id="projectinput2" class="form-control" placeholder="Enter second name" name="lname"/>
                        </div>
                        <div class="form-group">
                            <label for="projectinput3"><b>Email</b></label>
                            <input type="email" id="projectinput3" class="form-control" placeholder="example@abc.com" name="email"/>
                        </div>
                        <div class="form-group">
                            <label for="projectinput4"><b>Contact No.</b></label>
                            <input type="tel" id="projectinput4" class="form-control" placeholder="Contact no. " name="phone"/>
                        </div>
                        <div class="form-group">
                            <label for="projectinput5"><b>Messsage</b></label>
                            <textarea id="projectinput5" class="form-control" placeholder="message here......." name="message"></textarea>
                        </div>
                    <input id="BtnSub" type="button" class="submiteBtn btn btn-success" value="Submit"/>
                    <span id="error_msg" class="text-danger"></span>
                    <span id="success_msg" class="text-success"></span>
            </div>
        </form><br><br>
    </div>

    <div class="container">
        <a href="#" id="feedbacks"><u>hide feedbacks</u></a>
    </div>
    
    <div class="container">
        <div class="latest-comments-widget">
            <div class="section-heading">
                <h1>Latest Feedbacks</h1>
            </div>

            <div class="row">
                <!-- Single Comments -->
                <div class="comments">
                    @foreach($feedbacks as $feedback)
                        <div class="single-comments d-flex">
                            <div class="comments-text">
                            <a>{{$feedback->fname}} {{$feedback->lname}}</a>
                            <p>{{$feedback->message}}</p>
                            </div><br>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $("#BtnSub").click(function(){
                // console.log('asd');
                var fname=$("#projectinput1").val();
                var lname=$("#projectinput2").val();
                var email=$("#projectinput3").val();
                var phone=$("#projectinput4").val();
                var message=$("#projectinput5").val();
                if(fname==''||lname==''||email == ''||phone==''||message=='')
                {
                    $('#error_msg').css('display','block');
                    $('#error_msg').html('All fields are required').fadeOut(7000);
                }
                // console.log(fname);
                $.post('/feedback/store',{'_token':'{{csrf_token()}}', 'fname':fname, 'lname':lname, 'email':email, 'phone':phone, 'message':message},function(response){
                    console.log('test');
                    $('#success_msg').css('display','block');
                    $('#success_msg').html('Feedback posted successfully').fadeOut(7000);
                    $("#projectinput1").val('');
                $("#projectinput2").val('');
                $("#projectinput3").val('');
                $("#projectinput4").val('');
                $("#projectinput5").val('');
                });
            });
            $("#feedbacks").click(function(){
                // console.log('nire');
                $(".latest-comments-widget").hide();
            });
        });
    </script>
@endsection