@extends('frontend.layout.front')
@section('body')
<div class="comment-area section-padding-80-50">
    <div class="container">
        <div class="row">
        <div class="col-12 col-md-5 col-lg-5">
                <div class="section-heading">
                    <h1><b>Comments</b></h1>
                    <form method="GET" action='/comment/store' class="form">
                        {{csrf_field()}}
                        <div class="form-body">
                            <div class="form-group">
                                <label for="projectinput1"><b>Name</b></label>
                                <input type="text" id="projectinput1" class="form-control" placeholder="Enter name" name="name"/>
                            </div>
                        
                            <div class="form-group">
                                <label for="projectinput2"><b>Comment</b></label>
                                <textarea id="projectinput2" class="form-control" placeholder="Comments here......." name="comment"></textarea>
                            </div>
                            <input type="submit" class="btn btn-success" value="Submit"/>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>
@endsection