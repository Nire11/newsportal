<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Article;

class DashboardController extends Controller
{
    
    public function index()
    {
        $categories=Category::all();
        $articles=Article::all();

        // dd($categories);
        return view('dashboard',compact('categories','articles'));
    }
}
