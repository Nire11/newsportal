<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use Redirect;
use Session;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews=Review::paginate(2);
        return view('review.index',compact('reviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('review.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required|max:20',
            'designation'=>'required',
            'comment'=>'required'
        ]);
        $reviews=new Review;
        $reviews->name=$request->name;
        $reviews->designation=$request->designation;
        $reviews->comment=$request->comment;
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = time().'.'.$image->getClientOriginalExtension();
            $location = public_path('reviews_images/');
            $image->move($location,$filename);
            if($review->image)
            unlink(public_path('reviews_images/'.$review->image));
            $review->image=$filename;
        }
        $reviews->save();
        Session::flash('message','Sucessfully created reviews');
        return Redirect::to('/admin/reviews');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reviews=Review::findOrFail($id);
        return view('review.edit',compact('reviews'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|max:20',
            'designation'=>'required',
            'comment'=>'required'
        ]);
        $reviews=Review::findOrFail($request->id);
        $reviews->name=$request->name;
        $reviews->designation=$request->designation;
        $reviews->comment=$request->comment;if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = time().'.'.$image->getClientOriginalExtension();
            $location = public_path('reviews_images/');
            $image->move($location,$filename);
            $review->image=$filename;
        }
        $reviews->save();
        Session::flash('message','Edited Successfully');
        return Redirect::to('/admin/reviews');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $review=Review::findOrFail($id);
        if($review->image)
            unlink(public_path('reviews_images/'.$review->image));
        $review->delete();
        Session::flash('msg','Review deleted');
        return Redirect::to('/admin/reviews');
    }
}
