<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use Redirect;
use Session;
use Auth;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles=Article::paginate(5);
        Session::put('name',Auth::user()->name);
        return view('article.index',compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        return view('article.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'body'=>'required',
            'author'=>'required|max:50',
            'status'=>'required',
            'category_id'=>'required|integer'
        ]);
        $article=new Article;
        $article->title=$request->title;
        $article->body=$request->body;
        $article->author=$request->author;
        $article->status=$request->status;
        $article->category_id=$request->category_id;
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = time().'.'.$image->getClientOriginalExtension();
            $location = public_path('articles_images/');
            $image->move($location,$filename);
            if($article->image)
            unlink(public_path('articles_images/'.$article->image));
            $article->image=$filename;
        }
        $article->save();
        Session::flash('message','Article reated successfully');
        return Redirect::to('/admin/articles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category=Category::findOrFail($id);
        $articles=$category->articles;
        return view('article.show',compact('articles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories=Category::all();
        $articles=Article::findOrFail($id);
        return view('article.edit',compact('articles','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=>'required',
            'body'=>'required',
            'author'=>'required|max:50',
            'status'=>'required',
            'category_id'=>'required|integer',
            'image'=>'required'
        ]);
        $articles=Article::findOrFail($request->id);
        $articles->title=$request->title;
        $articles->body=$request->body;
        $articles->author=$request->author;
        $articles->status=$request->status;
        $articles->category_id=$request->category_id;
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = time().'.'.$image->getClientOriginalExtension();
            $location = public_path('articles_images/');
            $image->move($location,$filename);
            $article->image=$filename;
        }
        $articles->save();
        Session::flash('message','Edited Successfully');
        return Redirect::to('/admin/articles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article=Article::findOrFail($id);
        if($article->image)
            unlink(public_path('articles_images/'.$article->image));
        $article->delete();
        Session::flash('msg','Article deleted');
        return Redirect::to('/admin/articles');
    }
    // public function add($id)
    // {
    //     return view('article.addarticles')->with('category_id',$id);
    // }
    // public function articlesadded(Request $request)
    // {
    //     $article=new Article;
    //     $article->title=$request->title;
    //     $article->body=$request->body;
    //     $article->author=$request->author;
    //     $article->status=$request->status;
    //     $article->category_id=$request->category_id;
    //     $article->save();
    //     return Redirect::to('/categories'.'/'.$request->category_id.'/show');
    // }
    // public function view($id)
    // {
    //     $articles=Article::where('category_id',$id)->get();
    //     return view('article.viewarticles',compact('articles'));
    // }
    public function tooglestatus(Request $request)
    {
        $article=article::findOrFail($request->article_id);
        if ($article->status=='active')
            $article->status='inactive';
        else
            $article->status='active';
        $article->save();
        return $article->status;
    }
    public function search(Request $request){
        $articles=Article::where('title','LIKE','%'.$request->key.'%')->paginate(1);
        return view('article.index',compact('articles'));
    }

}