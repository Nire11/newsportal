<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Article;
use App\User;
use App\Phone;
use App\Role;
use Redirect;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        $categories=Category::paginate(5);
        return view('category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function create()
    {
        return view('category.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required|max:20',
            'description'=>'required'
        ]);
        $category=new Category;
        $category->name=$request->name;
        $category->description=$request->description;
        $category->save();
        Session::flash('message','Category added successfully');
        return Redirect::to('/admin/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $articles=Article::all();
        $articles=Article::findOrFail($id);
        // $categories=Category::findOrFail($id);
        return view('article.show',compact('articles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories=Category::findOrFail($id);
        return view('category.edit',compact('categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name'=>'required|max:20',
            'description'=>'required'
        ]);
        $categories=Category::findOrFail($request->id);
        $categories->name=$request->name;
        $categories->description=$request->description;
        $categories->save();
        Session::flash('message','Edited Successfully');
        return Redirect::to('/admin/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories=Category::findOrFail($id);
        $categories->delete();
        Session::flash('msg','Category deleted');
        return Redirect::to('/admin/categories');
    }
    public function showarticles($id)
    {
        $articles=Article::all();
        $articles=Article::where('category_id',$id)->get();
        
        return view('article.show',compact('articles'));
    }
    public function addarticles($id)
    {
        $articles=Article::all();
        
        return view('article.addarticles')->with('category_id',$id);
    }
    public function articlesadded(Request $request)
    {
        $article=new Article;
        $article->title=$request->title;
        $article->body=$request->body;
        $article->author=$request->author;
        $article->status=$request->status;
        $article->category_id=$request->category_id;
        $article->save();
        return Redirect::to('/admin/categories'.'/'.$request->category_id.'/showarticles');
    }
    public function tooglestatus(Request $request)
    {
        dd($request->id);
        $article=Article::findOrFail($request->id);
        if ($article->status=='active')
            $article->status='inactive';
        else
            $article->status='active';
        $article->save();
        return Redirect::to('/admin/categories'.'/'.$request->category_id.'/showarticles');
    }
}
