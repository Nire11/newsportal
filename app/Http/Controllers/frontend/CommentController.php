<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $comments=new Comment;
        $comments->name=$request->name;
        $comments->comment=$request->comment;
        $comments->article_id=$request->id;
        $comments->save();
        $view=view('frontend.partial.comment',compact('comments'))->render();
        return $view;
    }
}
