<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Feedback;
use Redirect;

class FeedbackController extends Controller
{
    public function index(){
        $feedbacks=Feedback::all();
        return view('frontend.contactus',compact('feedbacks'));
    }

    public function store(Request $request){
        // dd($request->all());
        // $request->validate([
        //     'fname'=>'required',
        //     'lname'=>'required',
        //     'email'=>'required',
        //     'message'=>'required'
        // ]);
        $feedback=new Feedback;
        $feedback->fname=$request->fname;
        $feedback->lname=$request->lname;
        $feedback->email=$request->email;
        $feedback->phone=$request->phone;
        $feedback->message=$request->message;
        $feedback->save();
        return 'success';
    }
    public function display($id)
    {
        $feedbacks=Feedback::all();
        $feedbacks=Feedback::findOrFail($id);
        return view('frontend.contactus',compact('feedbacks'));
    }
}
