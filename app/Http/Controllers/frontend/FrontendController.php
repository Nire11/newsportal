<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Article;
use App\Feedback;
use App\Comment;

class FrontendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories=Category::all();
        $articles=Article::all();
        $feedbacks=Feedback::all();

        $section1_articles=Article::all()->shuffle()->take(2);
        $section2_articles=Article::all()->shuffle()->take(4);
        $section3_articles=Article::all()->shuffle()->take(6);
        $section4_articles=Article::where('category_id','8')->get()->shuffle()->take(6);
        $section5_articles=Article::all()->shuffle()->take(8);
        // dd(Article::all());
        // dd($section1_articles);
        // dd($feedbacks);
        return view('frontend.index',compact('feedbacks','categories','articles','section1_articles','section2_articles','section3_articles','section4_articles','section5_articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getNews($slug)
    {
        $comments=Comment::all();
        $categories=Category::all();
        $cat=Category::where('slug',$slug)->firstOrFail();
        $articles=Article::where('category_id',$cat->id)->orderBy('created_at','desc')->paginate(3);
        $section_articles=Article::all()->shuffle()->take(8);
        // dd($articles);
        // $articles=Article::paginate(1);
        return view('frontend.news',compact('articles','comments','section_articles'));
    }
    public function search(Request $request){
        $categories=Category::where('name','LIKE','%'.$request->key.'%')->paginate(3);
        return view('frontend.list',compact('categories'));
    }
    public function articleShow($slug)
    {
        $comments=Comment::paginate(1);
        // dd($comments);
        $article=Article::where('slug',$slug)->firstOrFail();
        // dd($article);
        // $article=Article::findOrFail($slug);
        return view('frontend.article',compact('article','comments'));
    }
    public function test(Request $request)
    {
        return 'hello world';
    }
    public function like(Request $request)
    {
        $article=Article::findOrFail($request->id);
        $article->increment('count');
        return $article->count;
    }
}
