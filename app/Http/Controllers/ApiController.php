<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ApiController extends Controller
{
    public function api()
    {
        //request url
        $url = 'http://everestmediahouse.com/api/news';
        
        //create new instance of Client class
        $client = new Client();

        //send get request to fetch data
        $response = $client->request('GET', $url);

        //check response status ex: 200 is 'OK'
        if ($response->getStatusCode() == 200) {
            //header information contains detail information about the response.
            if ($response->hasHeader('Content-Length')) {
                //get number of bytes received
                $content_length = $response->getHeader('Content-Length')[0];
                echo '<p> Download '. $content_length. ' of data </p>';
            }

            //get body content
            $body =json_decode($response->getBody()->getContents());
            $news=$body->news;
            $collect= collect($news);
            $result_news=$collect->map(function($item,$key){
                    $item->news_image="/img.png";
                    return $item;
            });
            // dd($result_news);
            $section1=$collect->where('category_id',1);
            $section2=$collect->where('category_id',2);
            $section3=$collect->where('category_id',9);
            $section4=$collect->where('category_id',4);
            $section5=$collect->where('category_id',8);
            $section6=$collect->where('category_id',6);
            $section7=$collect->where('category_id',7);
            
            //  $xml = new \SimpleXmlElement($body);
            // dd($xml);
            //convert response to XML Element object
            // $xml = new \SimpleXmlElement($body);

            // //loop each item and display result
            // foreach($xml->entry as $item) {
            //     echo '<h3> Question By: ' . $item->author->name . '</h3>';
            //     echo '<p> Question: '. $item->summary . '</p>';
            // }
        }
        return view('frontend.othernews',compact('section1','section2','section3','section4','section5','section6','section7'))->with('news',$result_news);
    }
}
